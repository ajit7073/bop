//
//  RegistrationViewController.swift
//  bop
//
//  Created by apple on 13/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {

    @IBOutlet weak var hireTextField: UITextField!
    
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var hireView: UIView!
    
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    var workarray : [String] = ["Work","Hire"]
    var workpicker = UIPickerView()
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
       
registrationButton.layer.cornerRadius = 10
        hireView.layer.borderWidth = 1
        hireView.layer.borderColor = UIColor.black.cgColor
        hireTextField.delegate = self
        hireTextField.tag = 1
        mobileTextField.tag = 2
        emailTextField.tag = 3
        userNameTextField.tag = 4
        passwordTextField.tag = 5
        // Do any additional setup after loading the view.
        
        hideKeyboard()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

   
    @IBAction func registrationButtonAction(_ sender: Any) {
        postRegistrationCall()
    }
    @IBAction func loginButtonAction(_ sender: Any) {
    performSegue(withIdentifier: "segueRegistrationLogin", sender: nil)
    }
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        workpicker.isHidden = true
        view.endEditing(true)
    }
    
}

//MARK:-UITextFieldDelegate
extension RegistrationViewController:UITextFieldDelegate{
   
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            workpicker.isHidden = false
            workpicker.frame = CGRect(x: hireView.frame.origin.x, y: hireView.frame.origin.y + 30, width: hireView.frame.size.width, height: 100)
            workpicker.backgroundColor = UIColor.gray
            workpicker.layer.borderColor = UIColor.white.cgColor
            workpicker.layer.borderWidth = 1
            workpicker.delegate = self
            workpicker.dataSource = self
            self.view.addSubview(workpicker)
            return true
        }else {
             self.workpicker.removeFromSuperview()
            return false
            
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
}

//MARK:-UIPickerViewDeleget And DataSource

extension RegistrationViewController: UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1 //workarray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return workarray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return workarray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let value = workarray[row]
        hireTextField.text = value
        pickerView.isHidden = true
    }
}

extension RegistrationViewController {
    
    // MARK:- API ALERT MESSAGE FUNCTION
    
    func alertMessage(message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    //MARK:- API
    
    func postRegistrationCall(){
        let request = NSMutableURLRequest(url: NSURL(string: "https://webmastersinfotech.com/api/register_api.php")! as URL)
        request.httpMethod = "POST"
        let postString = "email=\(emailTextField.text!)&password=\(passwordTextField.text!)&name=\(userNameTextField.text!)&mobile=\(mobileTextField.text!)"
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: data!) as? [String:AnyObject]{
                    print(responseJSON)
                    print(responseJSON["success"]!)
                    
//                    //Check response from the sever
//                    let responds = responseJSON["success"]! as! Int
                    DispatchQueue.main.async {
//
//                        if responds == 1 {
                            self.performSegue(withIdentifier: "segueTabbar", sender: nil)
                            self.alertMessage(message: "Registration successfull!")
                        
                            //                         self.performSegue(withIdentifier: "segueLogin", sender: nil)
//                        }else {
//                            self.alertMessage(message:"Please Enter valid Your Data!")
//                        }
                    }
                    
                }
            }
            catch {
                print("Error -> \(error)")
            }
            
            
            
        }
        
        
        task.resume()
        
        
        
    }

}


