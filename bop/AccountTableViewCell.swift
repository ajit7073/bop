//
//  AccountTableViewCell.swift
//  bop
//
//  Created by apple on 22/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class AccountTableViewCell: UITableViewCell {
    @IBOutlet weak var nameAccountLabel: UILabel!
    @IBOutlet weak var valueAccountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
