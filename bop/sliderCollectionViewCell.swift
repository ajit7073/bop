//
//  sliderCollectionViewCell.swift
//  bop
//
//  Created by apple on 18/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class sliderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var sliderImageView: UIImageView!
}
