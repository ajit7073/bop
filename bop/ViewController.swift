//
//  ViewController.swift
//  bop
//
//  Created by apple on 13/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var sliderCollectionView: UICollectionView!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    var imgArr = [ "one",  "two", "three", "four", "five","six"] // this is files in assets
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupUI()
    }
    
    func setupUI() {
        if UserDefaults.standard.bool(forKey: "isLogin") {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "openTabbarSegue", sender: nil)
            }
            
        }
        let attributes = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributedText = NSAttributedString(string: loginButton.currentTitle!, attributes: attributes)
        
        loginButton.titleLabel?.attributedText = attributedText
        
        
        createAccountButton.clipsToBounds = true
        createAccountButton.layer.cornerRadius = 10
        sliderCollectionView.delegate = self
        sliderCollectionView.dataSource = self
        
    }
    
    
    @IBAction func createAccountButtonAction(_ sender: Any) {
        performSegue(withIdentifier: "segueRegistration", sender: nil)
    }
    @IBAction func loginButtonAction(_ sender: Any) {
        performSegue(withIdentifier: "segueLogin", sender: nil)
    }
    
    
}


extension ViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArr.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sliderCollectionViewCell", for: indexPath) as! sliderCollectionViewCell
        cell.sliderImageView.image = UIImage(named: imgArr[indexPath.row])
        cell.pageControl.currentPage = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        return CGSize(width:375, height:217)
    }
    
    // cell.cashOrCredit.image = UIImage(named: "1")
    
}




