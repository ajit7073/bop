//
//  MyProjectViewController.swift
//  bop
//
//  Created by apple on 21/02/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class MyProjectViewController: UIViewController {
    @IBOutlet weak var myProjectTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myProjectTableView.delegate = self
        myProjectTableView.dataSource = self

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK:- Tableview And TableViewCell

extension MyProjectViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "MyProjectTableViewCell"
        let cell = myProjectTableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 215
    }
}

